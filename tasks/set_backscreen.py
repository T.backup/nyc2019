import numpy
import cv2
from scipy import ndimage

# 背景の設定
def set_backscreen(img):

    WIDTH = 640*3
    HEIGHT = 320
    BLOCK = 16

    # (背景透過で)画像を取得
    # 呼び出した main.py からの相対座標で表現
    img_back = cv2.imread("images/back.png", cv2.IMREAD_UNCHANGED)

    # ラプラシアンフィルタ
    #karnel = numpy.array([[1,1,1],[1,-8, 1],[1,1,1]])
    #img_1 = cv2.filter2D(img_back, -1, karnel)

    # エッジ強調
    karnel = numpy.array([[0,-2.4,0],[-2.4,10,-2.4],[0,-2.4,0]])
    img_2 = cv2.filter2D(img_back, -1, karnel)

    # エッジの抽出
    #karnel = numpy.array([[0,-1,0],[-1,4.2,-1],[0,-1,0]])
    #img_3 = cv2.filter2D(img_back, -1, karnel)

    # 画像合成
    img_tmp = cv2.addWeighted(img_back, 0.4, img_2, 0.6, 0)

    # サイズ調整
    img_tmp = cv2.resize(img_tmp, (img.shape[1], img.shape[0]), interpolation=cv2.INTER_LINEAR)

    # 画像の貼り付け
    img[:,:,:3] += img_tmp[:,:,:]

    return img
