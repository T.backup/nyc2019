import numpy
import cv2
from scipy import ndimage

# 手前に丸太を並べる
def set_woods(img, woodList=[1,2,3,1,2,1,2,3,2,3]):

    WOOD_WIDTH = int(img.shape[1] / len(woodList))

    # 背景透過で画像を取得
    img_wood = cv2.imread("images/wood.png", cv2.IMREAD_UNCHANGED)

    # 画像の角度を変更
    img_wood = ndimage.rotate(img_wood,-2)

    # 不要な部分の切り捨て
    img_wood = img_wood[1000:2950, 715:1355, :]

    img_back = numpy.copy(img_wood)

    # エッジの抽出
    karnel = numpy.array([[0,-1,0],[-1,4.2,-1],[0,-1,0]])
    img_wood = cv2.filter2D(img_wood, -1, karnel)

    img_forward = numpy.copy(img_wood)

    # 画像合成
    img_wood = cv2.addWeighted(img_back, 0.6, img_forward, 0.4, 0)

    # 画像サイズを加工
    img_wood = cv2.resize(img_wood, (WOOD_WIDTH, 1000), interpolation=cv2.INTER_LINEAR)

    for i,height in enumerate(woodList):
        #wood_width = int(img.shape[1] / len(woodList))
        wood_x = i * WOOD_WIDTH
        wood_y = img.shape[0] - height*100
        img[wood_y:,wood_x:wood_x+WOOD_WIDTH ,:3] = img_wood[:height*100,:WOOD_WIDTH,:]
        #print(WOOD_WIDTH)
        #print(wood_x)
        #print(img[wood_y:,wood_x:wood_x+WOOD_WIDTH,:].shape, end="\t")
        #print(img_wood[:height*10,:WOOD_WIDTH,:].shape)

    return img

