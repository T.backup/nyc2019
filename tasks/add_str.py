import numpy
import cv2

# 文字列の描画
def add_str(img, TEXT):
    font = cv2.FONT_HERSHEY_SCRIPT_COMPLEX
    base_y = int(img.shape[0] / 2 - 430)
    base_x = 250

    # 文字を見やすくするため背景を白化
    img[base_y-120:base_y, base_x:base_x+1175,:] += 40

    # 文字列を img に追加
    # (~,~,~,255) を (~,~,~) のみに変えると背景がうまく透過されて美しくなる
    cv2.putText(img, TEXT, (base_x, base_y), font, 5, (0,0,255,255), 2, cv2.LINE_AA)

    return img
