import numpy
import cv2
import math

def set_cloud(img):

    # 背景透過で画像を取得
    img_cloud = cv2.imread("images/cloud.png", cv2.IMREAD_UNCHANGED)

    # 左右の不要な部分を切り取る
    img_cloud = img_cloud[:,1200:img_cloud.shape[1] - 650,:]

    # 上下反転
    img_cloud = cv2.flip(img_cloud, 0)

    # 横幅を基準に縦横比を維持してサイズを拡大縮小
    adjuster = img.shape[1] / img_cloud.shape[1]
    img_cloud = cv2.resize(img_cloud, (img.shape[1], int(img_cloud.shape[0]*adjuster)), interpolation=cv2.INTER_LINEAR)

    # 縦横幅を基準に比率で画像の切り抜き
    adjuster = int(img_cloud.shape[0] * (45/100))
    img_cloud = img_cloud[adjuster:adjuster+200,:,:]

    # 徐々にぼかしていきながら元画像に馴染ませていく
    for i in range(img_cloud.shape[0]):
        tmp = (img_cloud.shape[0] - i) / img_cloud.shape[0]
        tmp = tmp*9 + 1
        # 最大値が1になるように、log10(10)までの曲線を描く
        alpha = math.log10(tmp)
        #print(alpha)
        img[i,:,:3] = img[i,:,:3] * (1 - alpha) + img_cloud[i,:,:] *  alpha

    return img
