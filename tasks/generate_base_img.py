import numpy
import cv2

def _make_alpha(img):
    tmp = numpy.zeros((img.shape[0], img.shape[1], 4))
    tmp[:,:,:3] += img[:,:,:]
    img = tmp
    img[:,:,3] = 255
    return img

# 画像の基礎の生成
def generate_base_img(IMG_SIZE):
    img = numpy.zeros(IMG_SIZE, dtype=numpy.int32)
    img = _make_alpha(img)
    return img
