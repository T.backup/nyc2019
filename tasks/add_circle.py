import numpy
import cv2
import math

# alphaの追加
def _make_alpha(img_circle):
    tmp = numpy.zeros((img_circle.shape[0], img_circle.shape[1], 4))
    tmp[:,:,:3] += img_circle[:,:,:]
    img_circle = tmp
    # 透過度を0に
    img_circle[:,:,3] = 255
    return img_circle


# 円形の切り抜き
def _cut_circle(img_circle, R):
    tmp = numpy.zeros((2*R, 2*R, 3), dtype=numpy.int32)
    # 後に r を 0,1 で使用するため
    tmp = cv2.circle(tmp, (R, R), R, (1,0,0), -1)
    img_circle[:,:,3] = tmp[:,:,0]
    return img_circle


# sinカーブに沿って球を配置
def _on_sin(img, img_circle, ball_num=20):
    xList = []
    yList = []
    x_interval = img.shape[1] / ball_num

    # sinカーブに従いx,y座標のリストを作成
    for i in range(ball_num):
        # 始点をややずらす
        tmp_x = int(i*x_interval) - 50
        xList.append(tmp_x)
        tmp_y = int(abs(math.sin((i/ball_num) * (3.141592*4))) * 100) + 550
        yList.append(tmp_y)

    # x,y座標のリストに従い球を配置
    for x,y in zip(xList,yList):
        #img[y:y+img_circle.shape[0],x:x+img_circle.shape[1],:3] = img_circle[:,:,:3]
        for i in range(img_circle.shape[0]):
            for j in range(img_circle.shape[1]):
                # 配列外参照対策
                if (x + j <= 0 or img.shape[1] <= x + j):
                    continue
                # 円形外部の上書き対策
                if (img_circle[i,j,3] != 0):
                    img[y+i,x+j,:3] = img_circle[i,j,:3]
    return img


def add_circle(img, R=100):

    # 背景透過で画像を取得
    img_circle = cv2.imread("images/circle.png", cv2.IMREAD_UNCHANGED)

    # alphaの追加
    img_circle = _make_alpha(img_circle)

    # 使用する部分の正方形に成形
    img_circle = img_circle[10:10+2*R,720:720+2*R,:]

    # 円形の切り抜き
    img_circle = _cut_circle(img_circle, R)

    # 円形画像の合成
    img = _on_sin(img, img_circle)

    return img

