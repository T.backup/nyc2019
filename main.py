import numpy
import cv2

from tasks.generate_base_img import generate_base_img
from tasks.add_str import add_str
from tasks.set_backscreen import set_backscreen
from tasks.set_woods import set_woods
from tasks.set_cloud import set_cloud
from tasks.add_circle import add_circle

IMG_SIZE = (1181, 1748, 3)
TEXT = "Happy New Year"

def main():
    img = generate_base_img(IMG_SIZE)
    img = set_backscreen(img)
    img = set_cloud(img)
    img = set_woods(img)
    img = add_circle(img)
    img = add_str(img, TEXT)
    cv2.imwrite("output.png",img)

if __name__ == "__main__":
    main()
